package com.gspann.mqprocessor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.ErrorHandler;

@Service
public class JMSListenerErrorHandler implements ErrorHandler{

	private static Logger log = LoggerFactory.getLogger(JmsConfig.class);

	@Override
	public void handleError(Throwable t) {
		// TODO Auto-generated method stub
		log.error(t.getMessage());
		
	}

}
