package com.gspann.mqprocessor.dao;

import java.math.BigDecimal;
import java.util.Map;

public interface IInventoryDAO {
	
	Map<String,String> getSkuMap();

	void updateStocks(String productId, BigDecimal quantityValue, short stockStatus);
   
}
 