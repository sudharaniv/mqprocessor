package com.gspann.mqprocessor.dao;

import java.math.BigDecimal;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class InventoryDAO implements IInventoryDAO {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static Logger log = LoggerFactory.getLogger(InventoryDAO.class);

	public static String CATALOG_INVENTORY_STOCK_ITEM = "UPDATE cataloginventory_stock_item SET qty=?,is_in_stock=? WHERE product_id= ?";

	public static String CATALOG_INVENTORY_STOCK_STATUS = "UPDATE cataloginventory_stock_status SET qty=?,stock_status=? WHERE product_id= ?";

	public static String CATALOG_INVENTORY_STOCK_STATUS_IDX = "UPDATE cataloginventory_stock_status_idx SET qty=?,stock_status=? WHERE product_id= ?";

	@Override
	public Map<String, String> getSkuMap() {
		Map<String, String> skuMap = null;
		try {
			String sql = "SELECT row_id,sku FROM catalog_product_entity";
			skuMap = this.jdbcTemplate.query(sql, new SkuExtractor());
			return skuMap;
		} catch (Exception e) {
			log.error("Issues observed while populating skuMap");
			log.error(e.getMessage());
		}
		return skuMap;
	}

	@Override
	@Transactional
	public void updateStocks(String productId, BigDecimal quantityValue, short stockStatus) {
		try {
			jdbcTemplate.update(CATALOG_INVENTORY_STOCK_ITEM, quantityValue, stockStatus, productId);
			jdbcTemplate.update(CATALOG_INVENTORY_STOCK_STATUS, quantityValue, stockStatus, productId);
			jdbcTemplate.update(CATALOG_INVENTORY_STOCK_STATUS_IDX, quantityValue, stockStatus, productId);
			log.info("Done updating database for productId = " + productId);
		} catch (Exception e) {
			log.error("Issues observed while updating product " + productId);
			log.error(e.getMessage());
		}
	}

}
