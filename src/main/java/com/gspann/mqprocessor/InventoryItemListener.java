package com.gspann.mqprocessor;

import java.io.IOException;
import java.io.StringReader;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.gspann.dataprocessor.businessobjects.InventoryItem;
import com.gspann.dataprocessor.businessobjects.InventoryItemReceived;
import com.gspann.mqprocessor.service.IInventoryItemService;

@Component
public class InventoryItemListener {

	@Autowired
	private IInventoryItemService iInventoryItemService;

	private static Logger log = LoggerFactory.getLogger(InventoryItemListener.class);

	@JmsListener(destination = "${project.mq.queuename}", containerFactory = "jmsListenerContainerFactory")
	public String receiveMessage(final Message message) throws JMSException {
		log.info("7Theadname: " + Thread.currentThread().getName() + ", Thread Id: " + Thread.currentThread().getId());
		
		TextMessage messageData = (TextMessage)message;

		JAXBContext jc;
		try {
			jc = JAXBContext.newInstance(InventoryItem.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			StreamSource xmlSource = new StreamSource(new StringReader(messageData.getText()));
			InventoryItem inventoryItem = unmarshaller.unmarshal(xmlSource, InventoryItem.class).getValue();
			iInventoryItemService.processMessage(inventoryItem);

		} catch (JAXBException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return null;

	}
}
