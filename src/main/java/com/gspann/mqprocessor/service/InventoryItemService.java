package com.gspann.mqprocessor.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gspann.dataprocessor.businessobjects.InventoryItem;
import com.gspann.dataprocessor.businessobjects.InventoryItemReceived;
import com.gspann.mqprocessor.dao.IInventoryDAO;

@Service
public class InventoryItemService implements IInventoryItemService {

	@Autowired
	private IInventoryDAO inventoryDAO;

	private static Logger log = LoggerFactory.getLogger(InventoryItemService.class);

	Map<String, String> skuMap = new HashMap<String, String>();

	@Override
	public void processMessage(InventoryItem inventoryItem) {
		populateCatalogEntities();

		if (null == inventoryItem.getItemID()
				|| inventoryItem.getItemID().length() <= 0
				|| (null == skuMap || skuMap.size() == 0)) {
			log.info("Item id is not available in message received or skuMap has no entries");
			return;
		}

		BigDecimal quantity = inventoryItem.getAvailabilityChanges().getAvailabilityChange()
				.getOnhandAvailableQuantity();

		String sku = inventoryItem.getItemID();

		String productId = skuMap.get(sku);

		if (null == productId || productId.length() <= 0) {
			log.info("Product is not available for sku = " + sku);

			return;

		}

		log.info("sku = " + sku + " , productId = " + productId + " , quantity = " + quantity);

		short stockStatus = 1;
		if (quantity.intValue() <= 0) {
			quantity = new BigDecimal(0);
			stockStatus = 0;
		}
		log.info("updating stocks for product id = " + productId + " , quantityValue = " + quantity
				+ ", stockStatus = " + stockStatus);
		try {
			inventoryDAO.updateStocks(productId, quantity, stockStatus);
		} catch (Exception e) {
			log.error(e.getMessage());
			log.error("Error while updating stocks for product id = " + productId + " , quantityValue = "
					+ quantity + ", stockStatus = " + stockStatus);

		}

	}

	public void populateCatalogEntities() {
		if (null != skuMap && skuMap.keySet().size() > 0) {
			log.info("SKU MAP already populated, SIZE = " + skuMap.size());
			return;
		}
		skuMap = inventoryDAO.getSkuMap();
		log.info("SKU MAP SIZE = " + skuMap.size());

	}

}
