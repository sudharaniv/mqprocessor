package com.gspann.mqprocessor.service;

import com.gspann.dataprocessor.businessobjects.InventoryItem;
import com.gspann.dataprocessor.businessobjects.InventoryItemReceived;

public interface IInventoryItemService {

	void processMessage(InventoryItem inventoryItem);
	//TODO : remove below and make it private in service
    void populateCatalogEntities();
}
