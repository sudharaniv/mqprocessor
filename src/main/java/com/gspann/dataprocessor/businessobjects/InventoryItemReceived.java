package com.gspann.dataprocessor.businessobjects;

public class InventoryItemReceived {
	private InventoryItem InventoryItem;

	public InventoryItem getInventoryItem() {
		return InventoryItem;
	}

	public void setInventoryItem(InventoryItem InventoryItem) {
		this.InventoryItem = InventoryItem;
	}

	@Override
	public String toString() {
		return "ClassPojo [InventoryItem = " + InventoryItem + "]";
	}
}
