package com.gspann.dataprocessor.businessobjects;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="AvailabilityChanges")
public class AvailabilityChanges {
	
	@XmlElement(name="AvailabilityChange")
	private AvailabilityChange AvailabilityChange;

	public AvailabilityChange getAvailabilityChange() {
		return AvailabilityChange;
	}

	public void setAvailabilityChange(AvailabilityChange AvailabilityChange) {
		this.AvailabilityChange = AvailabilityChange;
	}

	@Override
	public String toString() {
		return "ClassPojo [AvailabilityChange = " + AvailabilityChange + "]";
	}
}