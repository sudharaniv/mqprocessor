package com.gspann.dataprocessor.businessobjects;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "InventoryItem")
public class InventoryItem {

	@XmlAttribute(name = "ItemID")
	private String ItemID;

	@XmlElement(name = "AvailabilityChanges")
	private AvailabilityChanges AvailabilityChanges;

	public String getItemID() {
		return ItemID;
	}

	public void setItemID(String ItemID) {
		this.ItemID = ItemID;
	}

	public AvailabilityChanges getAvailabilityChanges() {
		return AvailabilityChanges;
	}

	public void setAvailabilityChanges(AvailabilityChanges AvailabilityChanges) {
		this.AvailabilityChanges = AvailabilityChanges;
	}

	@Override
	public String toString() {
		return "InventoryItem [ItemID = " + ItemID + "]";
	}
}